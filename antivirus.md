
Please note:

- Disabling antivirus does not work, most modern antivirus programs continue scanning for certain types of software even when fully disabled.

- Windows Defender can never be fully removed, reguardless of what some registry changes may lead you to believe, much of the functionality of Windows Defender is embedded in system services like svchost.

- The software provided by cudo is only designed to monitor and control 3rd party mining software, cudo does not develop the actual miners themselves.

- Adding individual files does not work. The Cudo software regularly removes and replaces the binaries to be sure they are updated and to be sure they have not been compromised by malicious software.

- If you are using an unlisted antivirus, please report it so we may add a guide for it.<br/>
Meantime, in most cases you can simply just aff these two folder exclusions: <br/>`C:\ProgramData\Cudo Miner\`<br/>
`C:\Program Files\Cudo Miner\`


<br/>
This list is provided in alphabetical order, with exception for Windows Defender.
<br/><br/><hr/>
<summary><a href="https://gitlab.com/EternalBlueFlame/cudo-guides/-/blob/main/avguide/defender/README.md">Windows Defender</a></summary><br/>
<hr/>


<details><summary>Avast</summary><br/>
Images coming soon

- Open your Avast Antivirus and click on the `Menu` button in the upper-right corner.

- From the pop-out menu select `Settings`.

- From the menu on the left side, select `General`, then select `Exceptions` from the pop-out list. From this menu select `Add Exception`

- Using this method, add the following folders:<br/>
`C:\ProgramData\Cudo Miner\`<br/>
`C:\Program Files\Cudo Miner\`

</details><br/><hr/>

<details><summary>AVG</summary><br/>
Images coming soon

- Open your AVG Antivirus and click on the `Menu` button in the upper-right corner.

- From the pop-out menu select `Settings`.

- From the menu on the left side, select `General`, then select `Exceptions` from the pop-out list. From this menu select `Add Exception`

- Using this method, add the following folders:<br/>
`C:\ProgramData\Cudo Miner\`<br/>
`C:\Program Files\Cudo Miner\`

</details><br/><hr/>

<details><summary>Avira</summary><br/>
Images coming soon

- Open your Avira Antivirus and click on the `Security` button in the list on the left.

- From that menu select `Protection Options`.

- Click the small gear icon next to `System Protection`.

- From the menu on the top-left side, select `Real-time Protection`. From that drop-down select `Exceptions`.

- By putting the folder address in the bottom box, and then selecting `Add > >`<br/>Add the following folders:<br/>
`C:\ProgramData\Cudo Miner\`<br/>
`C:\Program Files\Cudo Miner\`

</details><br/><hr/>

<details><summary>BitDefender</summary><br/>
Guide coming soon
</details><br/><hr/>

<details><summary>Comodo</summary><br/>
Guide coming soon
</details><br/><hr/>

<details><summary>Dr.Web</summary><br/>
Guide coming soon
</details><br/><hr/>

<details><summary>Eset Nod32</summary><br/>
Guide coming soon
</details><br/>

<details><summary>F-Secure</summary><br/>
Guide coming soon
</details><br/><hr/>

<details><summary>Kaspersky</summary><br/>
Guide coming soon
</details><br/><hr/>

<details><summary>MalwareBytes</summary><br/>
Guide coming soon
</details><br/><hr/>

<details><summary>McAfee</summary><br/>
Guide coming soon
</details><br/><hr/>

<details><summary>Norton360</summary><br/>
Guide coming soon
</details><br/><hr/>

<details><summary>Panda</summary><br/>
Guide coming soon
</details><br/><hr/>

<details><summary>Sophos</summary><br/>
Guide coming soon
</details><br/><hr/>

<details><summary>Trend Micro</summary><br/>
Guide coming soon
</details><br/><hr/>

<details><summary>Trust Port</summary><br/>
Guide coming soon
</details><br/>
