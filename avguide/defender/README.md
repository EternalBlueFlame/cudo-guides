
Please note:

- Disabling antivirus does not work, most modern antivirus programs continue scanning for certain types of software even when fully disabled.

- Windows Defender can never be fully removed, reguardless of what some registry changes may lead you to believe, much of the functionality of Windows Defender is embedded in system services like svchost.

- The software provided by cudo is only designed to monitor and control 3rd party mining software, cudo does not develop the actual miners themselves.

- Adding individual files does not work. The Cudo software regularly removes and replaces the binaries to be sure they are updated and to be sure they have not been compromised by malicious software.<br/>


<br/><br/><hr/>
<summary>Windows Defender</summary><br/>

- Search out `Virus & Threat protection` in your start menu.<br/>
NOTE: To search in the windows 10 start menu, you need only open it and start typing.<br/>
<img src="https://gitlab.com/EternalBlueFlame/cudo-guides/-/raw/main/avguide/defender/1.jpg" />

- Select `Manage Settings`<br/>
<img src="https://gitlab.com/EternalBlueFlame/cudo-guides/-/raw/main/avguide/defender/2.jpg" />

- Select `Add or remove exclusions`<br/>
<img src="https://gitlab.com/EternalBlueFlame/cudo-guides/-/raw/main/avguide/defender/3.jpg" />

- Select `Add an exclusion` and then `Folder`<br/>
<img src="https://gitlab.com/EternalBlueFlame/cudo-guides/-/raw/main/avguide/defender/3.jpg" />

- Add the following folders:<br/>
`C:\ProgramData\Cudo Miner\`<br/>
`C:\Program Files\Cudo Miner\`

- If you have used a registry modification to hide or "remove" windows defender, you may need to restart the machine for these changes to take effect.

<br/><hr/>

