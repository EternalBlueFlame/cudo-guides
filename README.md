# Cudo Ambassador Guides

NOTICE: Remember that cudo staff and ambassadors will never ask for your password or for payment of services.


<hr/>
<details><summary>Troubleshooting</summary><br/>
<details><summary>Miner not working/All jobs failing</summary>
<br/>

- Be sure antivirus is setup correctly, including windows defender.
<br/>Disabling the antivirus, and/or adding exceptions for individual files <b>will not work.</b>
<br/>The registry hacks for disabling windows defender <b>do not work.</b>
<br/>Our guides to setting up each antivirus are listed here: <br/>[https://gitlab.com/EternalBlueFlame/cudo-guides/-/blob/main/antivirus.md](https://gitlab.com/EternalBlueFlame/cudo-guides/-/blob/main/antivirus.md)

- If you are running AMD and Nvidia cards in the same machine, you have to define what cards each miner uses, the detailed instructions for this are here:<br/>[https://gitlab.com/EternalBlueFlame/cudo-guides/-/blob/main/multiarch.md](https://gitlab.com/EternalBlueFlame/cudo-guides/-/blob/main/multiarch.md)

- If your GPU is under 5gb, then you can only have ETC and BTG miners used on that card, sometimes RVN works.

- If it's under 4gb then you can only have BTG enabled.

- IMPORTANT: If you are mining ETC, BTG, or RVN, be sure to enable auto-convert on the web dashboard under `settings > Payment`, since none of those can be withdrawn currently.<br/> You can also get to the auto-convert page by following this link: [https://console.cudominer.com/settings/payment](https://console.cudominer.com/settings/payment)

- If the card is 6gb or more, try setting the phoenix version from automatic to 5.5b, under advanced GPU settings on the main tab.

- To use the T-Rex miner on Nvidia cards you are required to use version 10.0 or 11.1 of the CUDA toolkit, which are available from the following link. NOTE: These versions are outdated and may not work as expected with 30 series cards.<br/>
[https://developer.nvidia.com/cuda-toolkit-archive](https://developer.nvidia.com/cuda-toolkit-archive)

- Some cards require overclocking enabled in the settings.

- You can try creating a new config for the organization, to be sure it's not a settings related issue
[https://gitlab.com/EternalBlueFlame/cudo-guides/-/blob/main/qeue.md](https://gitlab.com/EternalBlueFlame/cudo-guides/-/blob/main/qeue.md)

- In some extreme cases it may be due to driver error, when drivers update sometimes they leave behind invalid files from old versions, this is especially common when downgrading drivers. We have some more advanced guides on handling this stuff here:<br/>
[https://gitlab.com/EternalBlueFlame/cudo-guides/-/blob/main/ddu.md](https://gitlab.com/EternalBlueFlame/cudo-guides/-/blob/main/ddu.md)
</details>
</details>
<br/><hr/>
<details><summary>FAQ</summary>
<br/>
<details><summary>Why do the earnings fluctuate so much?</summary>
<br/>
The earnings are primarily based on three things:<br/>
- The transactions processed, which there's no real way to tell this aside from market trade volume, higher trade volume will mean more chances to process transactions and take a cut of the GAS fees.<br/>
- Shares accepted, this can also be random, they become harder to get as difficulty goes up. You get more chances to find them based on the hashrate you have.<br/>
- Currency Value, this one really only applies if you're using Auto-convert from one currency to another, but as the values for those currencies go up and down, so do the amounts you get.<br/>
</details>
</details>
<br/><hr/>
<details><summary>Pool mining with Cudo</summary>
Pool mining involves setting up a 3rd party miner to one of the ASIC pools, these do not actually require an ASIC.<br/>
You can find your Organization's ASIC pool login and address on the web dashboard at [https://console.cudominer.com/devices/setup](https://console.cudominer.com/devices/setup)
You can bring your own miner that's not listed here, but this is a list of official links to commonly used miners and their setup for an easy start.<br/><br/>
- Phoenix (GPU only): [https://bitcointalk.org/index.php?topic=2647654.msg56968316#msg56968316](https://bitcointalk.org/index.php?topic=2647654.msg56968316#msg56968316)
This is an example of a bash script for running Phoenix to the cudo pool. If you copy this be sure to correct the path and use your organization's ASIC pool info.
```bash
PhoenixMiner -pool stratum+tcp://stratum.cudopool.com:30005 -wal o:119601:n:MyWorker -pass x -worker MyWorker -logsmaxsize 20 -gpow 75 -tstop 61 -tstart 50
```
there are some additional settings in that at the end for you to fiddle with as well.
`-gpow` limits the max percent of GPU power used, this is good for multitasking.
`-tstart` and `-tstop` define the stop and restart points for mining, this is good to prevent overheating or other forms of thermal damage.
    NOTE: Most cards are designed for upwards of 90c, however due to various poor manufacturing tolerances in hardware, and close proximity to other parts in the case, 61 is the recommended max to be sure nothing melts or explodes.
<br/><br/>
- T-Rex (Nvidia only, requires CUDA toolkit):<br/>
Linux: [http://trex-miner.com/download/t-rex-0.20.3-linux.tar.gz](http://trex-miner.com/download/t-rex-0.20.3-linux.tar.gz)<br/>
Windows: [http://trex-miner.com/download/t-rex-0.20.3-win.zip](http://trex-miner.com/download/t-rex-0.20.3-win.zip)<br/>
Please note that T-Rex requires the CUDA Toolkit drivers, which are not shipped with standard or game ready drivers. These optional drivers can be obtained here: <br/>
[https://developer.nvidia.com/cuda-downloads](https://developer.nvidia.com/cuda-downloads)
```bash
t-rex --intensity 12 -a ethash -o stratum.cudopool.com:30004 -u o:119601:n:MyWorker -p x -w MyWorker --temperature-limit 60 --temperature-start 50
```
there are some additional settings in that at the end for you to fiddle with as well.
`-intensity` changes the mining priority, this is good for multitasking, but can hinder hashrate, this value goes from 10 to 25.
`-a` defines the currency being mined. The miner should figure this out on it's own, but it doesn't always.
`--temperature-limit` and `--temperature-start` define the stop and restart points for mining, this is good to prevent overheating or other forms of thermal damage.
    NOTE: Most cards are designed for upwards of 90c, however due to various poor manufacturing tolerances in hardware, and close proximity to other parts in the case, 61 is the recommended max to be sure nothing melts or explodes.
<br/><br/>
- Team Red Miner (AMD GPU only): [https://github.com/todxx/teamredminer/releases](https://github.com/todxx/teamredminer/releases)
<br/><br/>
- XMRig (Mainly aimed at CPU): [https://xmrig.com/download](https://xmrig.com/download)

</details>

