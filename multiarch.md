
To enable Multiarch support:

- Go to settings
- If you have a linked configuration, unlink it.
- Go to Advanced GPU settings
- Select a miner
- Under devices set the GPU ID's you wish to use for the Nvidia cards.
- Repeat this process for a different miner and enter the ID's for the AMD card(s).
NOTE:
- The ID's are defined by the PCI slot number, with some machines the first slot may be 0, or it may be 1. You may have to do some initial testing to find out.
- AMD and Nvidia GPU's can not be on the same miner, so it is recommended to use Team Red Miner for AMD, and Phoenix for Nvidia.
    in some cases T-Rex will work for Nvidia, and if it does depending on the GPU it may improve hashrate.

Here is an animation of the entire process provided by Not Marco
<img src= <https://images-ext-2.discordapp.net/external/0_m1s7PvECUE13r9GQOC3XgCfKkzu-p_edTlRviOkaQ/https/media.discordapp.net/attachments/833534198404349983/833534878041374720/kcKd2Ss0hg.gif />
